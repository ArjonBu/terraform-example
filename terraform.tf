#Variables

variable "aws_access_key" {}

variable "aws_secret_key" {}

variable "private_key_path" {}

variable "public_key_path" {}

variable "key_name" {
	default     = "ArjonBujupi"
}

variable "aws_region" {
	default     = "eu-central-1"
}


#Provider

provider "aws" {
	access_key = "${var.aws_access_key}"
	secret_key = "${var.aws_secret_key}"
	region = "${var.aws_region}"
}


#Resources
#This adds our key
resource "aws_key_pair" "ssh_public_key" {
  key_name   = "${var.key_name}"
  public_key = "${file(var.public_key_path)}"
}

#Creates a virtual private cloud for our infrastructure
resource "aws_vpc" "vpc" {
	cidr_block = "10.0.0.0/16"
}

#Create an internet gateway so we can have access to the internet
resource "aws_internet_gateway" "int_gateway" {
	vpc_id = "${aws_vpc.vpc.id}"
}

# Grant the vps internet access on its route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.vpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.int_gateway.id}"
}

#Creates a subnet which will be applied to all our resources
resource "aws_subnet" "subnet1" {
	vpc_id = "${aws_vpc.vpc.id}"
	cidr_block = "10.0.1.0/24"
	map_public_ip_on_launch = "true"
}

#Creates a security group for the load balancer
resource "aws_security_group" "load_balancer_sg" {
	name = "load-balancer-sg"
	vpc_id = "${aws_vpc.vpc.id}"

	ingress {
		from_port = 80
		to_port = 80
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}

	egress {
		from_port = 0
		to_port = 0
		protocol = "-1"
		cidr_blocks = ["0.0.0.0/0"]
	}
}

#Creates a security group for nginx and allows ssh traffic from outside but http traffic only from our virtual private cloud
resource "aws_security_group" "nginx_sg" {
	name = "nginx-sg"
	vpc_id = "${aws_vpc.vpc.id}"

	ingress {
		from_port = 80
		to_port = 8080
		protocol = "tcp"
		cidr_blocks = ["10.0.0.0/16"]
	}

	ingress {
		from_port = 22
		to_port = 22
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}

	egress {
		from_port = 0
		to_port = 0
		protocol = "-1"
		cidr_blocks = ["0.0.0.0/0"]
	}
}

#Creates the load balancer for our ec2 instance and forward traffic from port 80 on load balancer to port 8080 on our ec2 instance
resource "aws_elb" "nginx-elb" {
	name = "nginx-load-balancer"
	subnets = ["${aws_subnet.subnet1.id}"]
	security_groups = ["${aws_security_group.load_balancer_sg.id}"]
	instances = ["${aws_instance.nginx.id}"]

	listener {
		instance_port = 8080
		instance_protocol = "http"
		lb_port = 80
		lb_protocol = "http"
	}
}

#Creates our ec2 instance for nginx and applies the security group
resource "aws_instance" "nginx" {
	ami = "ami-d6696f3d"
	instance_type = "t2.micro"
	subnet_id = "${aws_subnet.subnet1.id}"
	key_name = "${var.key_name}"
	vpc_security_group_ids = ["${aws_security_group.nginx_sg.id}"]

	connection {
		user = "admin"
		private_key = "${file(var.private_key_path)}"
	}

#Here we add the docker repo and install all dependencies and then run our docker image with nginx and an index file
	provisioner "remote-exec" {
		inline = [
		  "sudo sudo apt-get update",
		  "sudo apt-get install -y apt-transport-https ca-certificates curl gnupg software-properties-common",
		  "curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -",
		  "sudo add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/debian stretch stable'",
		  "sudo apt-get update",
		  "sudo apt-get install -y docker-ce",
		  "sudo systemctl start docker",
		  "sudo systemctl enable docker",
		  "sudo docker run --name docker-nginx -p 8080:80 -d -v ~/html:/usr/share/nginx/html nginx",
		  "echo '<p>Hi</p>' | sudo tee --append ~/html/index.html"
		]
	}
	}

#Outputs the DNS name for our load balancer
output "aws_instance_public_dns" {
	value = "${aws_elb.nginx-elb.dns_name}"
}